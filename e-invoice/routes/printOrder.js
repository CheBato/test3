/*
 * DN3.6:a) Uporabite podatkovno bazo Northwind in tehnologijo ORM, da ustvarite e-račun na podlagi
 *          podane identifikacijske številke naročila.
 *
 *          Elektronsko račun naj pravilno prikaže postavke računa, vključno z atributi: 
 *          Opis, Obdobje Dobave (uporabite OrderDate in ShippedDate iz modela Orders),
 *          Količina, Cena (iz modela Products in ne OrderDetails), Vrednost, Znesek popusta, 
 *          DDV (pomeni znesek DDV), Osnova za DDV in Skupaj. Davčna stopnja (%) naj bo vedno enaka 22 %.
 *
 *          Vrednost popusta (%) tokrat pridobite iz podatkovne baze.
 *
 *          Za polno število točk uporabite knjižnico libxmljs, ustvarite nov element XML in ga
 *          vnesite v dokument XML.
 *
 *       b) Prav tako pravilno prikažite agregirane vrednosti: Vrednost postavk, Vsota popustov,
 *          Osnova za DDV, Vsota zneskov DDV, Vsota s popusti in davki in Za plačilo. Pri tem 
 *          nastavite SUPER RABAT na 0 %, tako da ne vpliva na končno plačilo.
 *          Kategorije Neobdavčeno, Zamudne obresti, Izravnava in Predplačilo naj imajo vrednost 0.
 *
 *       c) Prav tako pravilno prikažite povzetek davkov.
 *          (i)   Popravite splošno davčno stopnjo iz 20 % na 22 % in vrednosti Osnova za DDV, Vrednost DDV.
 *          (ii)  Popravite nižjo davčno stopnjo iz 8.5 % na 9.5 % in vrednosti Osnova za DDV, Vrednost DDV.
 *          (iii) Popravite neobdavčene vrednosti na 0.
 *
 *       d) Izpišite ime Fakturista (Zaposlenega)
 *
 *       e) Pravilno prikažite podatke prejemnika pošiljke. To so: Naziv podjetja, Naslov, Poštna št. in kraj,
 *          Država. Kodo države spremenite v XY. Nanašajte se na podatke iz tabele Orders.
 *
 *          Za polno število točk pri b), c) in d) spremenite obstoječo predlogo chkout.xml z uporabo knjižnice 
 *          libxmljs in tehnologije XPath.
 */

function datum(datum) {
	return datum.getDate() + ". " + datum.getMonth() + ". " + datum.getFullYear();
}

var fs = require('fs');
var xmljs = require("libxmljs");
var Sequelize = require('sequelize');
var sequelize = new Sequelize('northwind', 'nodejs', 'nodejs', {dialect: 'mysql',});
var Project = require('sequelize-import')(__dirname + '/../models', sequelize, {
    exclude: ['index.js']
});


Project.Orders.belongsTo(Project.Customers, {foreignKey: 'CustomerID'})
Project.Orders.hasMany(Project.OrderDetails, {foreignKey: 'OrderID'});
Project.OrderDetails.belongsTo(Project.Products, {foreignKey: 'ProductID'});
Project.Orders.belongsTo(Project.Employees, {foreignKey: 'EmployeeID'})





exports.printOrder = function(req, res){

	fs.readFile('./views/chkout.xml', function (err, data) {

		if (err) 
			throw err;

		content=data.toString(); 
		xmlDoc = xmljs.parseXml(content);


		// TODO

		res.header('Content-Type', 'application/xhtml+xml');

		
		console.log(req.query.order);
		
		Project.Orders
			.find({ where: { OrderID: req.query.order }, include: [{ model: Project.OrderDetails, include: [Project.Products,] },Project.Employees] })
				.success(function(qr) {

					if(qr == null)
						throw "errorNull";
					console.log(qr);
					
					var skpVrednost=0;
					var skpPopusti=0;
					var skpOsnovaDDV=0;
				    var skpZnesekDDV=0;
				    var skpSPopustiInDavki=0;
					var i=0;
					qr.OrderDetails.forEach(function(detail) {
						var DDV=22;
						var kolicina=detail.Quantity;
						var popust=detail.Discount;
						var cena=detail.Product.UnitPrice;
						var vrednost=cena*kolicina;
						var znesekPopusta=popust/100*vrednost;
						var osnovaDDV=(vrednost-znesekPopusta);
						var znesekDDV=(vrednost-znesekPopusta)*DDV/100;
						var skupaj=osnovaDDV+znesekDDV;
						
						skpVrednost+=vrednost;
						skpPopusti+=znesekPopusta;
						skpOsnovaDDV+=osnovaDDV;
						skpZnesekDDV+=znesekDDV;
						skpSPopustiInDavki+=skupaj;
						
						newElement = new xmljs.Element(xmlDoc, 'PostavkeRacuna')
						newElement
						
							.node('Postavka')
								.node('StevilkaVrstice', (i+1).toString()).parent() //TODO Nastavi Zap št.
							.parent()
							.node('DodatnaIdentifikacijaArtikla')
								.node('VrstaPodatkaArtikla', '5').parent()
								.node('StevilkaArtiklaDodatna', detail.Product.ProductID).parent() //TODO Nastavi Oznako
								.node('VrstaKodeArtiklaDodatna', 'SA').parent()
							.parent()
							.node('OpisiArtiklov')
								.node('KodaOpisaArtikla', 'F').parent()
								.node('OpisArtikla')
									.node('OpisArtikla1', detail.Product.ProductName).parent() //TODO Nastavi Opis
								.parent()
							.parent()
							.node('OpisiArtiklov')
								.node('OpisArtikla')
									.node('OpisArtikla1', 'OZNAKA_POSTAVKE').parent()
									.node('OpisArtikla2', 'navadna').parent()
								.parent()
							.parent()
							.node('OpisiArtiklov')
								.node('OpisArtikla')
									.node('OpisArtikla1', 'OPIS_ENOTE_MERE').parent()
									.node('OpisArtikla2', '').parent()
								.parent()
							.parent()
							
							.node('KolicinaArtikla')
								.node('VrstaKolicine', '47').parent()
								.node('Kolicina', kolicina.toString()).parent() //TODO Nastavi Količino
								.node('EnotaMere', 'PCE').parent()
							.parent()
							.node('CenaPostavke')
								.node('Cena', cena.toString()).parent() //TODO Nastavi Ceno
						.parent()
						
							.node('ZneskiPostavke')
								.node('VrstaZneskaPostavke', '203').parent()
								.node('ZnesekPostavke', vrednost.toString()).parent() //Nastavi Vrednost [CenaPostavke/Cena x KolicinaArtikla/Kolicina]
							.parent()
							
						.node('OdstotkiPostavk')
							.node('Identifikator', 'A').parent()
							.node('VrstaOdstotkaPostavke', '1').parent()
							.node('OdstotekPostavke', popust.toString()).parent() // Nastavi Popust (%)
							.node('VrstaZneskaOdstotka', '204').parent()
							.node('ZnesekOdstotka', znesekPopusta.toString()).parent() // Izračunaj Znesek Popusta [Vrednost x Popust (%)/100]
						.parent()
						
						.node('DavkiPostavke')
							.node('DavkiNaPostavki')
								.node('VrstaDavkaPostavke', 'VAT').parent()
								.node('OdstotekDavkaPostavke', DDV.toString()).parent() // Nastavi Davčno stop. (%)
							.parent()
							.node('ZneskiDavkovPostavke')
								.node('VrstaZneskaDavkaPostavke', '125').parent()
								.node('Znesek', osnovaDDV.toString()).parent()// Izračunaj Osnovo za DDV [Vrednost x (1-Popust (%)/100)]
							.parent()
							.node('ZneskiDavkovPostavke')
								.node('VrstaZneskaDavkaPostavke', '124').parent()
								.node('Znesek', znesekDDV.toString()).parent() //  Izračunaj DDV [Osnova za DDV x Davčna stop (%)/100]
							.parent()
						.parent()
							.node('ZneskiPostavke')
								.node('VrstaZneskaPostavke', '38').parent()
								.node('ZnesekPostavke', skupaj.toString()).parent() //  Izračunaj Skupaj [Osnova za DDV + DDV]
							.parent()
							
						.node('ReferencniDokumentiPostavke')
							.node('VrstaDokumentaPostavke', 'ON').parent()
							.node('StevilkaDokumentaPostavke', 'OIS-00000001').parent()
							.node('DatumDokumenta','2014-10-25T12:00:00Z').parent()
						.parent()
						.node('StroskovnoMesto', 'OIS-PE-001').parent()
						.node('ObdobjeDobave', datum(qr.OrderDate)+"-"+datum(qr.ShippedDate));
			
						
			
						// 7. The new element should be inserted into the e-invoice template at the appropriate place.
						//    We use XPath to navigate to the first element <PostavkaRacuna></PostavkaRacuna>
						//    [http://www.w3schools.com/xpath/]
						xppointer = '//PostavkeRacuna';
			
						// 8. Insert the new element after the i-th occurance of <PostavkaRacuna></PostavkaRacuna>
						xmlDoc.find(xppointer)[i].addNextSibling(newElement);
			
						// 9. Increment the item counter
						
						i ++;
					}
					);
					
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="79"]/ZneskiRacuna/ZnesekRacuna')[0].text(skpVrednost);
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="53"]/ZneskiRacuna/ZnesekRacuna')[0].text(skpPopusti);
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="125"]/ZneskiRacuna/ZnesekRacuna')[0].text(skpOsnovaDDV);
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="176"]/ZneskiRacuna/ZnesekRacuna')[0].text(skpZnesekDDV);
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="86"]/ZneskiRacuna/ZnesekRacuna')[0].text(skpSPopustiInDavki);
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="9"]/ZneskiRacuna/ZnesekRacuna')[0].text(skpSPopustiInDavki);
					xmlDoc.find('//GlobalniPopusti[TipPopusta=\'PP\']/OdstotekPopusta')[0].text(0);
					xmlDoc.find('//GlobalniPopusti[TipPopusta=\'PP\']/ZnesekPopusta')[0].text(0);

					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="131"]/ZneskiRacuna/ZnesekRacuna')[0].text(0);
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="165"]/ZneskiRacuna/ZnesekRacuna')[0].text(0);
					xmlDoc.find('//PovzetekZneskovRacuna[ZneskiRacuna/VrstaZneska="113"]/ZneskiRacuna/ZnesekRacuna')[0].text(0);
					
					xmlDoc.find('//DavkiRacuna[VrstaDavka=\'VAT\']/OdstotekDavka')[0].text(22);
			+		xmlDoc.find('//DavkiRacuna[VrstaDavka=\'VAT\']/OdstotekDavka')[1].text(9.5);
					
					xmlDoc.find('//ZneskiDavkov[VrstaZneskaDavka=\'125\']/ZnesekDavka')[0].text(skpOsnovaDDV);
					xmlDoc.find('//ZneskiDavkov[VrstaZneskaDavka=\'125\']/ZnesekDavka')[1].text(0);
					xmlDoc.find('//ZneskiDavkov[VrstaZneskaDavka=\'125\']/ZnesekDavka')[2].text(0);
					
					xmlDoc.find('//ZneskiDavkov[VrstaZneskaDavka=\'124\']/ZnesekDavka')[0].text(skpZnesekDDV);
					xmlDoc.find('//ZneskiDavkov[VrstaZneskaDavka=\'124\']/ZnesekDavka')[1].text(0);
					
					xmlDoc.find('//Besedilo[Tekst1=\'FAKTURIST\']/Tekst2')[0].text(qr.Employee.FirstName + " " + qr.Employee.LastName);
					
					
					xmlDoc.find('//NazivNaslovPodjetja[VrstaPartnerja=\'IV\']/NazivPartnerja/NazivPartnerja1')[0].text(qr.ShipName);
					xmlDoc.find('//NazivNaslovPodjetja[VrstaPartnerja=\'IV\']/Ulica/Ulica1')[0].text(qr.ShipAddress);
					xmlDoc.find('//NazivNaslovPodjetja[VrstaPartnerja=\'IV\']/PostnaStevilka')[0].text(qr.ShipPostalCode);
					xmlDoc.find('//NazivNaslovPodjetja[VrstaPartnerja=\'IV\']/Kraj')[0].text(qr.ShipCity);
					xmlDoc.find('//NazivNaslovPodjetja[VrstaPartnerja=\'IV\']/NazivDrzave')[0].text(qr.ShipCountry);
					xmlDoc.find('//NazivNaslovPodjetja[VrstaPartnerja=\'IV\']/KodaDrzave')[0].text("XY");
					
					
					res.header('Content-Type', 'application/xhtml+xml');
					res.end(xmlDoc.toString());
				})
				.error(function(err) {
					console.log(err);
				});

		

	});
};
