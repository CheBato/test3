
/* 
 * DN3.1: Razširite nalogo e6-index.js, tako da se bo za dan OrderID izpisalo tudi ime podjetja pod naročilom (tj. ime podjetja stranke).
 *
 * Primer izpisa.
 * ---
 * Order Date: Thu Jul 04 1996 02:00:00 GMT+0200 (CEST)
 * Product Name: Queso Cabrales, Ordered Quantity: 12
 * Product Name: Singaporean Hokkien Fried Mee, Ordered Quantity: 10
 * Product Name: Mozzarella di Giovanni, Ordered Quantity: 5
 * Company Name: Vins et alcools Chevalier
 */


var Sequelize = require('sequelize')
var sequelize = new Sequelize('northwind', 'nodejs', 'nodejs', {dialect: 'mysql',})
var Project = require('sequelize-import')(__dirname + '/models', sequelize, { exclude: ['index.js'] });



Project.Products.hasMany(Project.Orders, {foreignKey: 'ProductID', through: 'OrderDetails'});
Project.Orders.hasMany(Project.Products, {foreignKey: 'OrderID', through: 'OrderDetails'});
Project.Orders.belongsTo(Project.Customers,{foreignKey: 'CustomerID'});
// 2. Orders are associated to Products through OrderDetails     ^---------------------^

Project.Orders
	// 3. Find the order's date (and associtated products' names) where OrderID is 10248
	.find({ where: { OrderID: 10248 }, include: [Project.Products,Project.Customers]})
	.success(function(qr) {

		if(qr == null)
			throw "Err";
		//console.log(qr);

		console.log("---");
		console.log("Order Date: " + qr.OrderDate);
		qr.Products.forEach(function(ass) {
			//console.log(ass);
			console.log("Product Name: " + ass.ProductName);
		});
	    console.log("Company Name: "+qr.Customer.CompanyName);
		
		
		console.log('---')
	})
	.error(function(err) {
		console.log("Err"+err);
});


